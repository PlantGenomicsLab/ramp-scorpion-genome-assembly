# Scorpion Genome Assembly and Annotation
>- Working Directory: `/core/globus/cgi/RAMP`
>- [Google Spreadsheet](https://docs.google.com/spreadsheets/d/14q_x423iShvMXNqpiIYsy5PpFu_v6EXaqsK1v9szS0Y/edit#gid=413771353)
>- [Slides](https://drive.google.com/drive/u/0/folders/1KaO3Kpi-U_m_MU0TJ4gEFA2O_SNjYpn8)

[TOC]

## 1. Unfiltered ONT Raw Reads
>- Shear: `/core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/scorpion_ont.fastq.gz`
>- Ultra Long: '/core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/scorpion_ont_ultra.fastq.gz`

### Concatenate 'passing' fastq files
Script: `/core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/concat.sh`
```
shear=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023AUG27_RaMPHad_shear/basecalling/pass
ulmw=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023Aug27_RaMP_HadspULMW/basecalling/pass

cat $shear/*.fastq.gz > scorpion_ont.fastq.gz
cat $ulmw/*.fastq.gz > scorpion_ont_ultra.fastq.gz
```

### Convert fastq to fasta
Script: `/core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/fastq_to_fasta.sh`
```
module load seqtk/1.3

seqtk seq -a scorpion_ont.fastq.gz > scorpion_ont.fasta
seqtk seq -a scorpion_ont_ultra.fastq.gz > scorpion_ont_ultra.fasta
```

### Summary statistics with NanoPlot
Script: `/core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/nanoplot.sh`
```
dir=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023Aug27_RaMP_HadspULMW/basecalling
#dir=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023AUG27_RaMPHad_shear/basecalling

#ALL
NanoPlot --summary $dir/sequencing_summary.txt \
        --loglength \
        -o summary \
        -t 10

#PASS
awk 'NR == 1; NR > 1 && $10 == "TRUE"' $dir/sequencing_summary.txt > pass_summary.txt 
NanoPlot --summary pass_summary.txt \
        --loglength \
        -o summary-pass \
        -t 10

#FAIL
awk 'NR == 1; NR > 1 && $10 == "FALSE"' $dir/sequencing_summary.txt > fail_summary.txt 
NanoPlot --summary fail_summary.txt \
         --loglength \
         -o summary-fail \
         -t 10
```         

|                      | **Shear**          |                   |                   | **Ultra Long**   |                   |                  |
|----------------------|--------------------|-------------------|-------------------|-------------------|-------------------|------------------|
|                      | ALL (PASS + FAIL)  | PASS              | FAIL              | ALL (PASS + FAIL) | PASS              | FAIL             |
| Active channels:     | 2,706.00           | 2,664.00          | 2,696.00          |          2,413.00 |          2,350.00 |         2,375.00 |
| Mean read length:    | 7,304.80           | 7,297.80          | 7,341.10          |         18,585.10 |         19,151.90 |        16,049.50 |
| Mean read quality:   | 17.4               | 19.4              | 6.7               |              16.9 |              19.2 |              6.2 |
| Median read length:  | 4,172.00           | 4,141.00          | 4,316.00          |          6,215.00 |          6,483.00 |         5,182.00 |
| Median read quality: | 18.6               | 19.6              | 6.5               |              18.7 |              19.7 |              6.2 |
| Number of reads:     | 15,110,803.00      | 12,654,298.00     | 2,456,505.00      |      1,198,543.00 |        979,561.00 |       218,982.00 |
| Read length N50:     | 14,597.00          | 14,664.00         | 14,252.00         |         51,959.00 |         52,738.00 |        47,833.00 |
| STDEV read length:   | 8,352.00           | 8,161.60          | 9,270.90          |         28,573.70 |         28,749.50 |        27,632.10 |
| Total bases:         | 110,381,410,046.00 | 92,348,047,592.00 | 18,033,362,454.00 | 22,275,048,152.00 | 18,760,498,613.00 | 3,514,549,539.00 |

## 2. Filtered ONT Raw Reads

### Contaminant removal with Centrifuge
Database: fungi, bacterial, archea, viral


