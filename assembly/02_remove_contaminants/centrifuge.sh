#!/bin/bash
#SBATCH --job-name=centrifuge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G

module load centrifuge/1.0.4-beta

dir=/core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered
centrifuge -f \
        -x /core/labs/Wegrzyn/IngaGenome/Contam/longReads/f+b+a+v/abv \
        --report-file centrifuge_report.tsv \
        --quiet \
        --min-hitlen 50 \
        -p 12 \
        -U $dir/scorpion_ont_ultra.fasta

