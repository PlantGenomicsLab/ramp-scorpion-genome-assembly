#!/bin/bash
#SBATCH --job-name=removeclassified
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "Start"
module load seqkit

shear=/core/globus/cgi/RAMP/assembly/02_remove_contaminants/shear/*.out
ulmw=/core/globus/cgi/RAMP/assembly/02_remove_contaminants/ulmw/*.out

#taking the classified reads from the STDOUT file and adding those to a new file 
grep -vw "unclassified" $shear > shear_contaminated_reads.txt
grep -vw "unclassified" $ulmw > ulmw_contaminated_reads.txt

#extracting just the IDs from the text file
awk NF=1 shear_contaminated_reads.txt > shear_contaminated_read_ids.txt
awk NF=1 ulmw_contaminated_reads.txt > ulmw_contaminated_read_ids.txt

#keeping only unique IDs and removing any duplicates
sort -u shear_contaminated_read_ids.txt > shear_no_dup_centrifuge_contaminated_read_ids.txt
sort -u ulmw_contaminated_read_ids.txt > ulmw_no_dup_centrifuge_contaminated_read_ids.txt

#adds only the sequences that DO NOT match the headers in the file we provided
seqkit grep -v -f shear_no_dup_centrifuge_contaminated_read_ids.txt /core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/scorpion_ont.fastq.gz  > scorpion_ont.fastq.gz
seqkit grep -v -f ulmw_no_dup_centrifuge_contaminated_read_ids.txt /core/globus/cgi/RAMP/assembly/01_raw_reads/unfiltered/scorpion_ont_ultra.fastq.gz  > scorpion_ont_ultra.fastq.gz

echo "End"
