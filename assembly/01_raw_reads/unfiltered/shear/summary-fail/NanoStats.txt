General summary:         
Active channels:                   2,696.0
Mean read length:                  7,341.1
Mean read quality:                     6.7
Median read length:                4,316.0
Median read quality:                   6.5
Number of reads:               2,456,505.0
Read length N50:                  14,252.0
STDEV read length:                 9,270.9
Total bases:              18,033,362,454.0
Number, percentage and megabases of reads above quality cutoffs
>Q5:	2193430 (89.3%) 16051.0Mb
>Q7:	780916 (31.8%) 5121.0Mb
>Q10:	0 (0.0%) 0.0Mb
>Q12:	0 (0.0%) 0.0Mb
>Q15:	0 (0.0%) 0.0Mb
Top 5 highest mean basecall quality scores and their read lengths
1:	10.0 (7849)
2:	10.0 (22889)
3:	10.0 (1189)
4:	10.0 (565)
5:	10.0 (9706)
Top 5 longest reads and their mean basecall quality score
1:	1089080 (3.7)
2:	1065876 (3.1)
3:	746502 (3.8)
4:	624323 (3.3)
5:	608640 (5.4)
