#!/bin/bash
#SBATCH --job-name=fastq_to_fasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

module load seqtk/1.3

seqtk seq -a scorpion_ont.fastq.gz > scorpion_ont.fasta
seqtk seq -a scorpion_ont_ultra.fastq.gz > scorpion_ont_ultra.fasta


