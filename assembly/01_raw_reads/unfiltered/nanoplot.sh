#!/bin/bash
#SBATCH --job-name=Nanoplot
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=50G

module load NanoPlot/1.33.0

#dir=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023AUG27_RaMPHad_shear/basecalling
dir=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023Aug27_RaMP_HadspULMW/basecalling


#ALL
NanoPlot --summary $dir/sequencing_summary.txt \
        --loglength \
        -o summary \
        -t 10

#PASS
awk 'NR == 1; NR > 1 && $10 == "TRUE"' $dir/sequencing_summary.txt > pass_summary.txt 
NanoPlot --summary pass_summary.txt \
        --loglength \
        -o summary-pass \
        -t 10

#FAIL
awk 'NR == 1; NR > 1 && $10 == "FALSE"' $dir/sequencing_summary.txt > fail_summary.txt 
NanoPlot --summary fail_summary.txt \
         --loglength \
         -o summary-fail \
         -t 10
