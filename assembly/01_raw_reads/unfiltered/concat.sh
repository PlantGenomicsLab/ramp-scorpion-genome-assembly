#!/bin/bash
#SBATCH --job-name=concat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

#concatenate "passing" basecalled fastq files

shear=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023AUG27_RaMPHad_shear/basecalling/pass
ulmw=/core/globus/cgi/2023AUG22_RaMP_HadSp/2023Aug27_RaMP_HadspULMW/basecalling/pass

cat $shear/*.fastq.gz > scorpion_ont.fastq.gz
cat $ulmw/*.fastq.gz > scorpion_ont_ultra.fastq.gz
